# frozen_string_literal: true

require 'aws/ec2/metadata/version'

module Aws
  module EC2
    # Utility for accessing EC2 metadata on an instance in AWS.
    module Metadata
    end
  end
end
