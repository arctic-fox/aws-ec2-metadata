# frozen_string_literal: true

module Aws
  module EC2
    module Metadata
      VERSION = '0.1.0'
    end
  end
end
