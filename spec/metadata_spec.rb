# frozen_string_literal: true

RSpec.describe Aws::EC2::Metadata do
  it 'has a version number' do
    expect(Aws::EC2::Metadata::VERSION).not_to be nil
  end
end
